/** @file store_sensor_data.c
 *  @brief stores the sensor data in json file format
 *  
 *  This program stores the temperature and humidity data that is passed to it in a .json file
 *  
 *  @author Shriram K
 *  @note  Use this for storing temperature and humidity data that is read from DHT22 Sensor in a json file
 *  @bug  stores a single set successfully but when appending the next set. there's a error in json format
 *
 *
 */

/*
 *#####################################################################
 *  Initialization block
 *  ---------------------
 *  This block contains initialization code for this particular file.
 *#####################################################################
 */

/* --- Standard Includes --- */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* --- Function Prototypes --- */
void store_sensor_data(float,float);

/* MACROS */
#define FILE_NAME "/home/shriram/Code Workspace/sensor_data.json"



/* --- Global Variables --- */
FILE *fptr;

/*
 *#####################################################################
 *  Process block
 *  -------------
 *  
 *#####################################################################
 */

 /** 
 *  @brief store_sensor_data
 *  
 *  parses the json file and stores it in document
 *
 *  @return nothing
 */

void store_sensor_data(float t, float h)
{
    // variable to hold the time_stamp
    time_t seconds;
    //store the time since 00:00:00 UTC, January 1, 1970 (Unix timestamp) in seconds
    seconds = time(NULL);
    //Open the file
    fptr = fopen(FILE_NAME,"a");
    
    if(fptr == NULL)
    {
        printf("Error!");   
        exit(1);             
    }
    
    fprintf(fptr,"{\n");
    fprintf(fptr,"\t\"time_stamp\": \"%ld\",",seconds);
    fprintf(fptr,"\n");
    fprintf(fptr,"\t\"temperature\": \"%f\",",t);
    fprintf(fptr,"\n");
    fprintf(fptr,"\t\"humidity\": \"%f\"",h);
    fprintf(fptr,"\n");
    fprintf(fptr,"}\n");
    
    //Close the file
    fclose(fptr);

}

/** 
 *  @brief main function
 *  
 *  main entry point of the program
 *
 *  @return 0 
 */
int main()
{
    //test the function with dummy values
    store_sensor_data(32.0,56.5);
    store_sensor_data(42.5,76.2);
    
    return 0;
}
